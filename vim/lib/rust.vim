Plug 'rust-lang/rust.vim'

let g:formatdef_rustfmt = '"rustfmt"'
let g:formatters_rust = ['rustfmt']
let g:ale_rust_cargo_use_check = 1
